package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/RegisterPetServlet")
public class RegisterPetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");  
        PrintWriter out = response.getWriter();           
        String nameEmail = request.getParameter("unameoremail");   
        String password = request.getParameter("psw");
        String confirm_password =request.getParameter("confirm_password");
        try
        {  
        	String connectionURL = "jdbc:mysql://localhost:3306/petshop";
 
        	Class.forName("com.mysql.cj.jdbc.Driver").newInstance();

        	Connection con = DriverManager.getConnection(connectionURL, "root", "Vcetvcet@35");

  
             PreparedStatement ps=con.prepareStatement("insert into pettable(unameoremail,psw,confirm_password) values(?,?,?)");  

             ps.setString(1,nameEmail);  
             
             if(password.equals(confirm_password))
             {
            	 ps.setString(2,password);  
                 ps.setString(3, confirm_password); 
                 int i = ps.executeUpdate();  
           /*      response.sendRedirect("http://localhost:8085/new/PetLoginRegister.jsp");*/
                 if(i>0)  
                 {
            	 RequestDispatcher rd=request.getRequestDispatcher("PetLoginRegister.jsp");  
			     rd.forward(request, response);
                 }
             }
             
             else
             {
            	 out.println("Sorry password and confirm password must be same <a href='RegisterPet.jsp' ><h4>try again</h4></a>");
            	/* out.print("Sorry password and confirm password must be same!");  
                 RequestDispatcher rd=request.getRequestDispatcher("/RegisterPet.jsp"); 
             	
                 rd.include(request, response); */
             }
        }
        catch (Exception ex)
        {
             ex.printStackTrace();
        }  
        out.close();  
   }  
	}