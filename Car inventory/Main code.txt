/*
 mysql> create table tbl(id int unique,make varchar(20),model varchar(20),year int,price double);

 */
package projects;
import java.sql.*;
import java.util.*;

public class Car {		
	static Connection con;
	static PreparedStatement ps;
	static Statement st;
	static ResultSet rs ;
    
	public static void addCarDetails() 
	{	
    	 try {   			 
    		 Scanner sc2 = new Scanner(System.in);
    		 System.out.print("\nEnter id :");
    		 int id=sc2.nextInt();
    		 
             System.out.print("Enter make :");
             String make=sc2.next();
             
             System.out.print("Enter model :");
             String model = sc2.next();
             
             System.out.print("Enter year :");
             int year=sc2.nextInt();
             
             System.out.print("Enter price($) :");
             double price=sc2.nextDouble();
             
             con =ConnectionsClass.getConnection() ;
          
             ps = con.prepareStatement("insert into tbl values(?,?,?,?,?)");
             ps.setInt(1,id);
             ps.setString(2,make);  
             ps.setString(3,model);    
             ps.setInt(4,year);
             ps.setDouble(5,price);                         
             ps.executeUpdate();   
             System.out.println("\nAdded successfully");          
} 
catch (Exception e) 
{
             System.out.println(e);
}
finally
{
    try {
         ps.close();
	     con.close();
       } 
    catch (SQLException e) 
    {
	e.printStackTrace();
    }          
   }
  }
		
	public static void deleteCarDetails()
	{
		Scanner sc = new Scanner(System.in);
			
        System.out.print("Enter id you want to delete :");
        int id=sc.nextInt();
        boolean check=Car.idCheck(id);
        if(check)
        {
        	try {
        	con =ConnectionsClass.getConnection() ;
    		st = con.createStatement();
    		ps = con.prepareStatement("delete from tbl where id = ?");
    		ps.setInt(1,id);  
    		ps.executeUpdate();
    		System.out.println("\nDeleted successfully");   	    
    		} 
    		catch (Exception e)
    		{
    		System.out.println(e);
    		} 
       		finally 
    		{
    		try {
      			st.close();
    			con.close();
    		}
    		catch (SQLException e) 
    		{
    			e.printStackTrace();
    		}   		
    		}
        }
        else
        {
        	System.out.println("ID not present in the table.Try with another id");
        }
	}
	
	
	public static void updateCarDetails()
	{
		Scanner sc = new Scanner(System.in);

        System.out.print("Enter id :");
        int upId=sc.nextInt();
        boolean check=Car.idCheck(upId);
        if(check)
        {
        try {
        System.out.print("Press 1 : Update Make\n"
        		       +"Press 2 : Update Model\n"
        		       +"Press 3 : Update Year\n"
        		       +"Press 4 : Update Price\n");
        
        System.out.print("Enter your choice :");        
        int press=sc.nextInt();
		con =ConnectionsClass.getConnection() ;
		st = con.createStatement();
		
	  switch(press)
   	   {
          case 1:
        	  System.out.print("Enter make you want to update :");
              String upMake=sc.next();
        	  ps = con.prepareStatement("update tbl SET make = ? where id = ?");
      		  ps.setString(1,upMake); 
      		  ps.setInt(2,upId);
      		  ps.executeUpdate();
      		  System.out.println("\nUpdated successfully");
      		  break;
          case 2:
        	  System.out.print("Enter model you want to update :");
              String upModel=sc.next();
        	  ps = con.prepareStatement("update tbl SET model = ? where id = ?");
      		  ps.setString(1,upModel); 
      		  ps.setInt(2,upId);
      		  ps.executeUpdate();
      		  System.out.println("\nUpdated successfully");
      		  break;
          case 3:
        	  System.out.print("Enter year you want to update :");
              int upYear=sc.nextInt();
        	  ps = con.prepareStatement("update tbl SET year = ? where id = ?");
      		  ps.setInt(1,upYear); 
      		  ps.setInt(2,upId);
      		  ps.executeUpdate();
      	 	  System.out.println("\nUpdated successfully");
      		  break;
          case 4:
        	  System.out.print("Enter price you want to update :");
              double upPrice=sc.nextDouble();
        	  ps = con.prepareStatement("update tbl SET price = ? where id = ?");
      		  ps.setDouble(1,upPrice); 
      		  ps.setInt(2,upId);
      		  ps.executeUpdate();
      	      System.out.println("\nUpdated successfully");
      		  break;
          }
		}
		catch (Exception e)
		{
		System.out.println(e);
		} 		
		finally 
		{
		try {
			
			st.close();
			con.close();
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}		}  }
        else
        {
        	System.out.println("ID not present in the table.Try with another id");
        }		
	}

	public static void listOfCarDetails () 
	{
		int countRows=0;
		double Total_inventory=0;
		try {
		con =ConnectionsClass.getConnection() ;
		st = con.createStatement();
		rs = st.executeQuery("select * from tbl");
		
		boolean empty = true;
		while (rs.next()) 
		{
	    System.out.println("------------------------------------");
		System.out.println("| "+rs.getInt(1)+" | "+rs.getString(2)+" | "+rs.getString(3)+" | "+rs.getInt(4)+" | "+rs.getDouble(5)+" |");
	    countRows++;
	    Total_inventory=rs.getDouble(5)+Total_inventory;
		empty = false;
		}
		 System.out.println("------------------------------------");
		 System.out.println("\nNumber of cars: "+countRows);
		 System.out.println("Total_inventory :$"+Total_inventory);
		if(empty) 
		{
			System.out.println("There are currently no cars in the catalog.");
        }
		}
		catch (Exception e)
		{
		System.out.println(e);
		} 
		finally
		{
		try {
			rs.close();
			st.close();
			con.close();
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}		
		}
		}
		
	public static boolean idCheck(int id)
	 {			
			con =ConnectionsClass.getConnection() ;
			try {
				st = con.createStatement();
				rs = st.executeQuery("select * from tbl");
				while (rs.next()) 
				{
					if(rs.getInt(1)==id)
					{
						return true;
					}
				}
			} catch (SQLException e) {
		
				e.printStackTrace();
			}
			return false;			
	}	
	
	public static void main(String[] args){	
		
		int countCar=0;
		Scanner sc=new Scanner(System.in);		
		
       System.out.println("\tWelcome to Car Garage!\n");
       System.out.println("\nThese are all the commands which you can perform");

       int i=0;
       while(i!=1)
       {
    	   System.out.println("\n*Add\n*Delete\n*Update\n*List");
    	   System.out.print("\nEnter command : " );
    	
    	   String str = sc.nextLine().toLowerCase();
    	   if(str.equals("quit"))
    	   {
    			   System.out.println("\nGood Bye!");	
    			   break;   		  
    	   }
    	   else if(str!="quit")
    	   {
    		   switch(str)
        	   {
               case "add":
            	   countCar++;
                   if(countCar>20)
            	   {
            		   System.out.println("\nThere is no space in database.");
            		   countCar--;
            		   i++;
            		   break;
            	   }
                 Car.addCarDetails();
                   continue;
               case "list":
                 Car.listOfCarDetails();
                   continue;
               case "delete":
                  Car.deleteCarDetails();
                   countCar--;
                   continue;
               case "update":
                  Car.updateCarDetails();
                   continue;
               default:
                   System.out.println("\nSorry, but "+str+" is not a valid command. Please try again.");
                   continue;
               } 
           }            
       }      
   }
}
