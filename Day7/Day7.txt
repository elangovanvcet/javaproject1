                                  Day 7 Java lab exercise


1.Write a java program to print current date and time in the specified format.

sol:


package Day6;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateAndTime {

	public static void main(String[] args) {

		// Instantiate a Date object
	      Date date = new Date();

	      SimpleDateFormat ft = new SimpleDateFormat ("E yyyy/MM/dd 'at' hh:mm:ss ");

	      System.out.println("Current Date: " + ft.format(date));

	}

}


o/p: Current Date: Sun 2021/05/16 at 12:54:29 


2.Write a Java program to extract date, time from the date string

sol:

package Day6;

class ExtractDateTimeFromString {

	// Function to get day, month, and
	// year from date
	public static void findDate(String date,String time)
	{
		// Spiliting the given date by '-'
		String dateParts[] = date.split("-");
		String timeParts[] = time.split(":");

		String day = dateParts[0];
		String month = dateParts[1];
		String year = dateParts[2];
		
		String hours = timeParts[0];
		String minutes = timeParts[1];
		String seconds = timeParts[2];
		
		System.out.println("Day: " +day);
		System.out.println("Month: " +month);
		System.out.println("Year: " +year);

		System.out.println("Hours: " +hours);
		System.out.println("Minutes: " +minutes);
		System.out.println("Seconds: " +seconds);
	}

	public static void main(String args[])
	{
		String date = "18-07-2020";
		
		String time= "7:10:11.555";

		findDate(date, time);
		
	}
}


o/p:

Day: 18
Month: 07
Year: 2020
Hours: 7
Minutes: 10
Seconds: 11.555


3.Write a Java program to get and display information 
(year, month, day, hour, minute) of a default calendar

sol:

package Day6;

import java.util.*;
public class DefaultCalendar {
	public static void main(String args[])
	{
		Calendar c = Calendar.getInstance();
		System.out.println("The Current Date is:" + c.getTime());
	}
}

o/p

The Current Date is:Sun May 16 13:03:07 IST 2021


4.Write a Java program to get the maximum value of the year, month, week, date 
from the current date of a default calendar

sol:

package Day6;


import java.util.*;
public class MaxValueOfYearMonthDateWeek {
	public static void main(String[] args)
	{
		
		// creating calendar object
		Calendar calendar = Calendar.getInstance();
		System.out.println("YEAR : "+calendar.getActualMaximum(Calendar.YEAR));
		System.out.println("MONTH : "+calendar.getMaximum(Calendar.MONTH));
		System.out.println("WEEK_OF_YEAR : "+calendar.getActualMaximum(Calendar.WEEK_OF_YEAR));
		System.out.println("DATE : "+calendar.getActualMaximum(Calendar.DATE));
	}
	}




o/p

YEAR : 292278994
MONTH : 11
WEEK_OF_YEAR : 52
DATE : 31


5..Given two inputs year and month (Month is coded as: Jan=0, Feb=1 ,Mar=2 ...), 
write a program to find out total number of days in the given month for the given year.

sol:


package Day6;
import java.util.Calendar;
import java.util.Scanner;


public class TotalNoDaysInMonth {
		public static void main(String[] args) {
			
			Scanner sc=new Scanner(System.in);
			System.out.println("Enter the year");
			int year=sc.nextInt();
			System.out.println("Month is coded as: Jan=0, Feb=1 ,Mar=2 ...");
			int month=sc.nextInt();
		
			
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.YEAR, year);
			calendar.set(Calendar.MONTH, month);
			int numDays = calendar.getActualMaximum(Calendar.DATE);
			System.out.println(numDays);
		}

	}

o/p

Enter the year
2021
Month is coded as: Jan=0, Feb=1 ,Mar=2 ...
5
30




6.Include a class UserMainCode with a static method “getNumberOfDays” that accepts 2 integers as arguments and returns an integer. The first argument corresponds to the year and the second argument corresponds to the month code. The method returns an integer corresponding to the number of days in the month.

Create a class Main which would get 2 integers as input and call the static method getNumberOfDays present in the UserMainCode.

Input and Output Format:

Input consists of 2 integers that correspond to the year and month code.

Output consists of an integer that correspond to the number of days in the month in the given year.

Sample Input:

2000

1

Sample Output:

29


sol:

package Day6;
import java.util.Calendar;
import java.util.Scanner;

public class GetNoOfDays {

	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		int year=sc.nextInt();
		int month=sc.nextInt();
	
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month);
		int numDays = calendar.getActualMaximum(Calendar.DATE);
		System.out.println(numDays);
	}

}

o/p

2000
1
29




7.Write a program to read a string  and to test whether first and last character are same. The string is said to be be valid if the 1st and last character are the same. Else the string is said to be invalid.

Include a class UserMainCode with a static method checkCharacters which accepts a string as input .

The return type of this method is an int.  Output should be 1 if the first character and last character are same . If they are different then return -1 as output.

Create a class Main which would get the input as a string and call the static method checkCharacters present in the UserMainCode.

 

Sample Input :

the picture was great

Sample Output : Valid  

Sample Input :

this

Sample Output :

Invalid



sol:


package Day6;

//characters of a string are equal or not.
class CheckChar {
      int checkCharacter(String s)
	{
		int n = s.length();
		if (n < 2)
		return -1;
		if (s.charAt(0) == s.charAt(n-1))
		return 1;
		else
		return 0;
	}
}
public class CheckCharacters {
	public static void main(String[] args)
	{
		String s = "the picture was great";
		CheckChar cc= new CheckChar();
		int res = cc.checkCharacter(s);
		if (res == -1)
			System.out.println("Invalid Input");
		else if (res == 1)
			System.out.println("valid");
		else
			System.out.println("Invalid");
	}

}

o/p   valid



8. Write a code to read two int array lists of size 5 each as input and to merge the two arrayLists, sort the merged arraylist in ascending order and fetch the elements at 2nd, 6th and 8th index into a new arrayList and return the final ArrayList.

 Include a class UserMainCode with a static method sortMergedArrayList which accepts 2 ArrayLists.

 The return type is an ArrayList with elements from 2,6 and 8th index position .Array index starts from position 0.

 Create a Main class which gets two array list of size 5 as input and call the static method sortMergedArrayList present in the UserMainCode.

Sample Input:

3 1 17 11 19 5 2 7 6 20

Sample Output:

3 11 19

Sample Input :

1 2 3 4 5 6 7 8 9 10

Sample Output :

3 7 9


sol:

package Day6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FinalArrayList {

	public static void main(String[] args) {

		  List<Integer> ls1 = new ArrayList<Integer>();
		  ls1.add(3);
		  ls1.add(1);
		  ls1.add(17);
		  ls1.add(11);
		  ls1.add(19);

		  System.out.println("list 1 values : " + ls1);
		  
		  List<Integer> ls2 = new ArrayList<Integer>();
		  ls2.add(5);
		  ls2.add(2);
		  ls2.add(7);
		  ls2.add(6);
		  ls2.add(20);
	

		  System.out.println("\nlist 2 values : " + ls2);
		  
		  
		  List<Integer> ls3 = new ArrayList<Integer>();
		  ls3.addAll(ls1);
		  ls3.addAll(ls2);
		  
		  System.out.println("\nMerged list 3 values : " + ls3);
		  
		  Collections.sort(ls3);
		  
		  System.out.println("\nSorted List : " + ls3);
		  
		  ls3.add(2, 55);
		  ls3.add(6, 65);
		  ls3.add(8, 75);
		  
		  System.out.println("\nAdded Elements in Final ArrayList : " + ls3.get(2) + " "+ ls3.get(6)+" "+ ls3.get(8) );
	
		  System.out.println("\nAfter added elements in the FinalList : " + ls3);
	}

}


o/p

list 1 values : [3, 1, 17, 11, 19]

list 2 values : [5, 2, 7, 6, 20]

Merged list 3 values : [3, 1, 17, 11, 19, 5, 2, 7, 6, 20]

Sorted List : [1, 2, 3, 5, 6, 7, 11, 17, 19, 20]

Added Elements in Final ArrayList : 55 65 75

After added elements in the FinalList : [1, 2, 55, 3, 5, 6, 65, 7, 75, 11, 17, 19, 20]





9. A Company wants to obtain employees of a particular designation. You have been assigned as the programmer to build this package. You would like to showcase your skills by creating a quick prototype. The prototype consists of the following steps:

Read Employee details from the User. The details would include name and designaton in the given order. The datatype for name and designation is string.

Build a hashmap which contains the name as key and designation as value.

You decide to write a function obtainDesignation which takes the hashmap and designation as input and returns a string array of employee names who belong to that designation as output. Include this function in class UserMainCode.

Create a Class Main which would be used to read employee details in step 1 and build the hashmap. Call the static method present in UserMainCode.

Sample Input 1:

4

Manish

MGR

Babu

CLK

Rohit

MGR

Viru

PGR

MGR

Sample Output 1:

Manish

Rohit




sol:


package Day6;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;


class Employee
{
	String[] obtainDesignation(Map<String, String> hmap, String newDesignation)
	{
		String[] newArr= new String[hmap.size()];
		int i=0;
		Iterator<Entry<String,String>> iter=hmap.entrySet().iterator();
		while(iter.hasNext())
		{			
			String key=iter.next().getKey();
			String value=hmap.get(key);
			
			if(value.equals(newDesignation))
			{			
				newArr[i]=key;
				i++;
			}
		}
		return newArr;		
	}
}
public class EmployeeDetails {

	public static void main(String[] args) {


		Map<String,String> hmap=new HashMap<String,String>();
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the Number of Employee");
		int n=sc.nextInt();
	
		 for (int i = 0; i < n; i++) 
		 {
			    System.out.println("Enter the Name of Employee  : ");
	            String name = sc.next();
	            System.out.println("Enter the Designation of Employee");
	            String designation = sc.next();
	            
	            hmap.put(name, designation);	            
	      }
		 System.out.println("Enter the Search Designation");
		 String newDesignation = sc.next();
		 Employee e=new  Employee();
		 String[]newStr=(e.obtainDesignation(hmap, newDesignation));
		 
		 for (int i = 0; i < newStr.length; i++) 
		 {
			 if(newStr[i]!=null)
			 {
				 System.out.println(newStr[i]);    
			 }
			 else
			 {
				 continue;
			 }
	      }

	}

}


o/p


Enter the Number of Employee
4
Enter the Name of Employee  : 
manish
Enter the Designation of Employee
MGR
Enter the Name of Employee  : 
Viru
Enter the Designation of Employee
CLK
Enter the Name of Employee  : 
Hari
Enter the Designation of Employee
MGR
Enter the Name of Employee  : 
jordan
Enter the Designation of Employee
PGR

Enter the Search Designation
MGR


Hari
manish



10.Write a program to read two String variables in DD-MM-YYYY.Compare the two dates and return the older date in 'MM/DD/YYYY' format.

Include a class UserMainCode with a static method findOldDate which accepts the string values. The return type is the string.

Create a Class Main which would be used to accept the two string values and call the static method present in UserMainCode.

Sample Input 1:

05-12-1987

8-11-2010

Sample Output 1:

12/05/1987


sol:

package Day6;

public class ReturnOlderDate {

	public static void main(String[] args) {
		
		 String fdate="05-12-1987";
	     String sdate="8-11-2010";
	     
	     String diff[] = fdate.split("-");
		 String diff1[] = sdate.split("-");
		 
	     int y1=Math.abs(Integer.parseInt(diff[2]));
	     int y2=Math.abs(Integer.parseInt(diff1[2]));
	     if(y1<y2)
	     {
	    	 System.out.println(diff[1]+"/"+diff[0]+"/"+diff[2]);
	     }
	     else
	     {
	    	 System.out.println(diff1[1]+"/"+diff1[0]+"/"+diff1[2]);
	     }

	}

}


o/p

12/05/1987



11.Given a method with two date strings in yyyy-mm-dd format as input. Write code to find the difference between two dates in months. 

Include a class UserMainCode with a static method getMonthDifference which accepts two date strings as input. 

The return type of the output is an integer which returns the diffenece between two dates in months. 

Create a class Main which would get the input and call the static method getMonthDifference present in the UserMainCode.

 

Sample Input 1:

2012-03-01

2012-04-16

Sample Output 1:

1

Sample Input 2:

2011-03-01

2012-04-16

Sample Output 2:

13


sol:


package Day6;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

class UserMainCode4
{
	 long getDateDifference(String str,String str1)
	{
		String diff[] = str.split("-");
		String diff1[] = str1.split("-");
	
		int year1=Math.abs(Integer.parseInt(diff[0]));
		int month1=Math.abs(Integer.parseInt(diff[1]));
		int days1=Math.abs(Integer.parseInt(diff[2]));
		
		int year2=Math.abs(Integer.parseInt(diff1[0]));
		int month2=Math.abs(Integer.parseInt(diff1[1]));
		int days2=Math.abs(Integer.parseInt(diff1[2]));
		
		LocalDate firstData = LocalDate.of(year1, month1, days1);
		LocalDate secondData = LocalDate.of(year2, month2, days2);

		// calculate difference
		long months = ChronoUnit.MONTHS.between(firstData ,secondData);
		
		return months;
	
	    

	}
}
public class DiffBtYearDateMonth {

	public static void main(String[] args) {

	   UserMainCode4 uc = new  UserMainCode4();
       String fdate="2011-03-01";
       String sdate="2012-04-16";
       System.out.print("Months between " + fdate + " and " + sdate + ": "+ uc.getDateDifference(fdate, sdate));
       
		
	}

}


o/p

Months between 2011-03-01 and 2012-04-16: 13


12.Write a program to read a string and validate the IP address. Print “Valid” if the IP address is valid, else print “Invalid”.

 

Include a class UserMainCode with a static method ipValidator which accepts a string. The return type (integer) should return 1 if it is a valid IP address else return 2.

Create a Class Main which would be used to accept Input String and call the static method present in UserMainCode.

Note: An IP address has the format a.b.c.d where a,b,c,d are numbers between 0-255.

 

Sample Input 1:

132.145.184.210

Sample Output 1:

Valid

 

Sample Input 2:

132.145.184.290

Sample Output 2:

Invalid



sol:


package Day6;

class UserMainCode6
		{
			int validateNumber(String str)
			{
				String validation[] = str.split("-");
				if(validation[0].length()== 3&& validation[1].length()==3
						&&validation[2].length()==3 &&validation[3].length()==3)
				{
					return 1;
			     }
				else
					return -1;
			}
		}
public class IpAddressValidation {

			public static void main(String args[])
			{
				UserMainCode6 uc=new UserMainCode6();
				String str= "132.145.184.210";
				String str1=str.replace('.', '-');
				int k=uc.validateNumber(str1);
				if(k==1)
					System.out.println("Valid ");
				else
					System.out.println("InValid ");
			}
		}

o/p   Valid




13.Get two date strings as input and write code to find difference between two dates in days.

Include a class UserMainCode with a static method getDateDifference which accepts two date strings as input.

The return type of the output is an integer which returns the diffenece between two dates in days.

Create a class Main which would get the input and call the static method getDateDifference present in the UserMainCode.

 

Sample Input 1:

2012-03-12

2012-03-14

Sample Output 1:

2

Sample Input 2:

2012-04-25

2012-04-28

Sample Output 2:

3


sol:


package Day6;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

class UserMainCode3
{
	 long getDateDifference(String str,String str1)
	{
		String diff[] = str.split("-");
		String diff1[] = str1.split("-");
	
		int year1=Math.abs(Integer.parseInt(diff[0]));
		int month1=Math.abs(Integer.parseInt(diff[1]));
		int days1=Math.abs(Integer.parseInt(diff[2]));
		
		int year2=Math.abs(Integer.parseInt(diff1[0]));
		int month2=Math.abs(Integer.parseInt(diff1[1]));
		int days2=Math.abs(Integer.parseInt(diff1[2]));
		
		LocalDate firstData = LocalDate.of(year1, month1, days1);
		LocalDate secondData = LocalDate.of(year2, month2, days2);

		// calculate difference
		long days = ChronoUnit.DAYS.between(firstData , secondData);

		return days;
	
	    

	}
}
public class DifferenceBetweenDate {

	public static void main(String[] args) {

	   UserMainCode3 uc = new  UserMainCode3();
       String fdate="2012-04-25";
       String sdate="2012-04-28";
       System.out.println( "Days between " + fdate+ " and " + sdate + ": " + uc.getDateDifference(fdate, sdate));
       
		
	}

}

o/p

Days between 2012-04-25 and 2012-04-28: 3



14.Given a date string in the format dd/mm/yyyy, write a program to convert the given date to the format dd-mm-yy.

Include a class UserMainCode with a static method “convertDateFormat” that accepts a String and returns a String.

 Create a class Main which would get a String as input and call the static method convertDateFormat present in the UserMainCode.

Sample Input:

12/11/1998

 

Sample Output:

12-11-98



sol:

package Day6;

class UserMainCode1
{
	void convertDateFormat(String str)
	{
		String convert[] = str.split("/");
		System.out.println(convert[0] + "-" + convert[1] +"-" + convert[2].substring(2, 4) );

	}
}

public class ConvertDate {

	public static void main(String[] args) {

		UserMainCode1 uc1=new UserMainCode1();
		String str= "12/11/1998";
		uc1.convertDateFormat(str);
		}

	}

o/p  12-11-98



15.Number Validation

Write a program to read a string of 10 digit number , check whether the string contains a 10 digit number in the format XXX-XXX-XXXX where 'X' is a digit.

Include a class UserMainCode with a static method validateNumber which accepts a string as input .

The return type of the output should be 1 if the string meets the above specified format . In case the number does not meet the specified format then return -1 as output.

Create a class Main which would get the input as a String of numbers and call the static methodvalidateNumber present in the UserMainCode.

Input and Output Format:

Input consists of a string.

Output is a string specifying the given string is valid or not .

Refer sample output for formatting specifications.

Sample Input 1:

123-456-7895

Sample Output 1:

Valid number format

Sample Input 2:

-123-12344322

Sample Output 2:

Invalid number format



sol:


package Day6;


class UserMainCode
{
	int validateNumber(String str)
	{
		String validation[] = str.split("-");
		if(validation[0].length()==3&&validation[1].length()==3&&validation[2].length()==4)
		{
			return 1;
	     }
		else
			return -1;
	}
}
public class CheckValidation {

	public static void main(String args[])
	{
		UserMainCode uc=new UserMainCode();
		String str= "123-456-7995";
		int k=uc.validateNumber(str);
		if(k==1)
			System.out.println("Valid number format");
		else
			System.out.println("InValid number format");
	}
}

o/p  Valid number format



16.Fetching Middle Characters from String

Write a program to read a string of even length and to fetch two middle most characters from the input string and return it as string output.

Include a class UserMainCode with a static method getMiddleChars which accepts a string of even length as input . The return type is a string which should be the middle characters of the string.

Create a class Main which would get the input as a string and call the static method getMiddleCharspresent in the UserMainCode.

Input and Output Format:

Input consists of a string of even length.

Output is a string .

Refer sample output for formatting specifications.

Sample Input 1:

this

Sample Output 1:

hi

Sample Input 1:

Hell

Sample Output 1:

el



sol:


package Day6;

class MiddleChars
{
	void getMiddleChars(String str)
	{
		int len = str.length();
        // Finding middle index of string
        int middle = len / 2; 
        // Print the middle character
        // of the string
        if(len%2==0)
        {
        	 System.out.println(str.charAt(middle-1)+""+str.charAt(middle));
        }
        else
        {
        	 System.out.println(str.charAt(middle));
        }
	}
}
public class UserMainCode {

public static void main(String[] args) {
					    
	            String str= "this";				
				MiddleChars mc=new MiddleChars();
				mc.getMiddleChars(str);	    
			}
}

o/p

hi






17.Write a program to read a string array, remove duplicate elements and sort the array. Note:

1. The check for duplicate elements must be case-sensitive. (AA and aa are NOT duplicates)

2. While sorting, words starting with upper case letters takes precedence.

Include a class UserMainCode with a static method orderElements which accepts the string array. The return type is the sorted array. Create a Class Main which would be used to accept the string arrayand integer and call the static method present in UserMainCode. Input and Output Format: Input consists of an integer n which is the number of elements followed by n string values. Output consists of the elements of string array. Refer sample output for formatting specifications. Sample Input 1: 6 AAA BBB AAA AAA CCC CCC Sample Output 1: AAA BBB CCC Sample Input 2: 7 AAA BBB aaa AAA Abc A b Sample Output 2: A AAA

Abc BBB aaa b


sol:

package Day6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class RemoveDuplicateString {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter no of strings");
		int k=sc.nextInt();
		Set<String>set=new TreeSet<String>();
		for(int i=0;i<k;i++)
		{
			set.add(sc.next());
		}
		
		List<String>ls=new ArrayList<>();
		ls.addAll(set);
		Collections.sort(ls);
		System.out.println(ls);
	}

}

o/p

Enter no of strings
6
AAA
BBB
AAA
AAA
CCC
CCC
[AAA, BBB, CCC]


18.String Splitter

Write a program which would accept a string and a character as a delimiter. Apply the below rules 1. Using the delimiter, split the string and store these elements in array. 2. Reverse each element of the string and convert it into lowercase. Include a class UserMainCode with a static method manipulateLiteral which accepts the string and character. The return type is the string array formed. Create a Class Main which would be used to accept the string and characterand call the static method present in UserMainCode. Input and Output Format: Input consists of a string and character. 
Output consists of a string array. Refer sample output for formatting specifications. 
Sample Input 1: AAA/bba/ccc/DDD / Sample Output 1: aaa abb ccc ddd


sol:

package Day6;

import java.util.Scanner;

public class StringSpliter
{
	public static void main(String[] args)
    {
        String reverseString = "";
        
        System.out.println("Enter string to reversed");
        
        //Read the input from the user
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        
        // Split sentence into seperate words
        String[] words = input.split("/"); 
        
        // Iterate the String array
        for(String word : words)
        {
        	// Append each reversed Words
        	reverseString = reverseString + new StringBuilder(word).reverse().toString()+" ";
        }
  
        System.out.println("Reversed String : "+reverseString.toLowerCase());
    }
}

o/p

Enter string to reversed
AAA/bba/ccc/DDD
Reversed String : aaa abb ccc ddd 

